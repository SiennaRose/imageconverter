import java.util.Scanner;
import java.io.IOException;

/**
 * Driver program: Converts a color image into various colors
 * @author cs108
 *
 */
public class ImageConverterTester {

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        ImageConverter ic = new ImageConverter();
        int x=60,y=60;  // test pixel values

        String filename = scan.next();
        ic.readImage(filename);
        System.out.println(ic.getPixelRGB(x, y));
        ic.toGrayscale();
        System.out.println(ic.getPixelRGB(x, y));
        ic.writeImage();

        // TODO: Re-read in image, convert to Red; repeat for Green and Blue
        String filename2 = scan.next();
        ic.readImage(filename2);
        System.out.println(ic.getPixelRGB(x, y));
        ic.toRed();
        System.out.println(ic.getPixelRGB(x, y));
        ic.writeImage();
        
        String filename3 = scan.next();
        ic.readImage(filename3);
        System.out.println(ic.getPixelRGB(x, y));
        ic.toGreen();
        System.out.println(ic.getPixelRGB(x, y));
        ic.writeImage();
        
        String filename4 = scan.next();
        ic.readImage(filename4);
        System.out.println(ic.getPixelRGB(x, y));
        ic.toBlue();
        System.out.println(ic.getPixelRGB(x, y));
        ic.writeImage();
    }

}