import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Program 4b: ImageConverter
 * Reads in an image file, converts it's colors from grayscale, red,
 * blue, green. Then writes toa new file
 * CS108-2
 * @author Sienna Sacramento
 *
 */
public class ImageConverter 
{
	private BufferedImage img; 
	private String color; 
	private String inputFilename; 
	
	/**
	 * Defualt Constuctor 
	 */
	ImageConverter()
	{
		
	}
	
	/**
	 * Reads in the file 
	 * @param filename
	 * @throws IOException
	 */
	public void readImage(String filename) throws IOException
	{
		inputFilename= filename; 
		
		try 
		{
			File f1= new File(inputFilename);
			img = ImageIO.read(f1);
		}
		catch(IOException e)
		{
			throw e; 
		}
	}
	
	/**
	 * makes image into grayscale version by finding average of RGB values
	 * 
	 */
	public void toGrayscale()
	{
		for(int y= 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
			{
				int p = img.getRGB(x, y); 
				
				int a = (p>>24) &0xff;
				int r = (p>>16) &0xff;
				int g = (p>>8) &0xff;
				int b = p&0xff; 
				
				//calculate average
				int avg = (r+g+b) / 3; 
				
				//replace RGB value with avg
				p = (a<<24) | (avg<<16) | (avg<<8) | avg; 
				
				img.setRGB(x, y, p);
			}
		color = "gray";
	}
	
	/**
	 * makes image into red conversion of itself
	 */
	public void toRed()
	{
		for(int y= 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
			{
				int p = img.getRGB(x, y); 
				
				int a = (p>>24) &0xff;
				int r = (p>>16) &0xff;
				//int g = (p>>8) &0xff;
				//int b = p&0xff;  
				
				//replace RGB value with avg
				p = (a<<24) | (r<<16) | (0<<8) | 0; 
				
				img.setRGB(x, y, p);
			}
		color = "red";
	}
	
	/**
	 * makes image a green conversion of itself
	 */
	public void toGreen()
	{
		for(int y= 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
			{
				int p = img.getRGB(x, y); 
				
				int a = (p>>24) &0xff;
				//int r = (p>>16) &0xff;
				int g = (p>>8) &0xff;
				//int b = p&0xff;  
				
				//replace RGB value with avg
				p = (a<<24) | (0<<16) | (g<<8) | 0; 
				
				img.setRGB(x, y, p);
			}
		color = "green";
	}
	
	/**
	 * makes image a blue conversion of itself
	 */
	public void toBlue()
	{
		for(int y= 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
			{
				int p = img.getRGB(x, y); 
				
				int a = (p>>24) &0xff;
				//int r = (p>>16) &0xff;
				//int g = (p>>8) &0xff;
				int b = p&0xff;  
				
				//replace RGB value with avg
				p = (a<<24) | (0<<16) | (0<<8) | b; 
				
				img.setRGB(x, y, p);
			}
		color = "blue";
	}
	
	/**
	 * save converted image into a new file
	 */
	public void writeImage()
	{
		try 
		{
			File outputFile = new File(inputFilename + color + ".jpg");
			ImageIO.write(img, "jpg", outputFile);
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
	}
	
	/**
	 * displays the rgb value int int form of the pixel at (x,y)
	 * 
	 * @param x		x coordinate of the pixel
	 * @param y		y coordinate of the pixel
	 */
	public String getPixelRGB(int x, int y)
	{
		Color c = new Color(img.getRGB(x,y), true); 
		return "RGB: " + c.getRed() + ":" + c.getGreen() + ":" + c.getBlue(); 
	}
	
	/**
	 * author and # of program 
	 * @return String 
	 */
	public String getIdentificationString()
	{
		return "Program 4b, Sienna Sacramento";
	}
}
